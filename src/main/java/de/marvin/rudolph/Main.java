package de.marvin.rudolph;

import de.marvin.rudolph.manager.SetupManager;
import de.marvin.rudolph.manager.UsbWatcher;
import lombok.Getter;

@Getter
public class Main {

    @Getter
    private static Main instance;

    private SetupManager setupManager;

    public Main() {
        Main.instance = this;

        this.setupManager = new SetupManager();
        new UsbWatcher();
    }

    public static void main(String[] args) {
        new Main();
    }
}
