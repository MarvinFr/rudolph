package de.marvin.rudolph.manager;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

public class ProcessManager extends Thread {


    public ProcessManager() {
        run();
    }

    @Override
    public void run() {
        File from = new File(SetupManager.getInstance().getUsbPath());
        File to = new File(SetupManager.getInstance().getBackUpPath());
        try {
            copyFilesInDirectory(from, to);
        } catch (IOException ex) {
            System.err.println("Fehler!");
            System.exit(1);
        }

        try {
            sleep(1000 * 3600);
            System.out.println("5 seconds are over");
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            run();
        }
    }

    private void copyFilesInDirectory(File from, File to) throws IOException {
        if (!to.exists()) {
            to.mkdirs();
        }
        System.out.println(from);
        for (File path : Objects.requireNonNull(from.listFiles())) {
            File file = new File(to.getAbsolutePath() + "/" + path.getName());
            if (path.isDirectory()) {
                copyFilesInDirectory(path, file);
            } else {
                Files.copy(path.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
}
