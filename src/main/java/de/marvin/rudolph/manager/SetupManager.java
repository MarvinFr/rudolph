package de.marvin.rudolph.manager;

import de.marvin.rudolph.util.FileUtil;
import de.marvin.rudolph.util.JsonConfiguration;
import lombok.Getter;

import java.util.Scanner;

@Getter
public class SetupManager {

    @Getter
    private static SetupManager instance;

    private final JsonConfiguration jsonConfiguration;

    private String usbPath;
    private String backUpPath;

    public SetupManager() {
        SetupManager.instance = this;

        this.jsonConfiguration = new JsonConfiguration(FileUtil.createFile("/Users/marvin/Documents/test/", "settings.json"));

        if (!ready()) {
            System.out.println("Hey mein Name ist Rudolph dein persoenlicher BackUP Assistent!");
            System.out.println("So wie's aussieht musst du noch einen PATH festlegen, damit ich deine Dateien von deinem USB in einen BackUP PATH speichern kann");
            System.out.println("Bitte gebe nun den Pfad ein wo dein USB-Stick liegt (z.B.: C:/Intenso/): ");
            Scanner usbPathScanner = new Scanner(System.in);
            this.usbPath = usbPathScanner.next();
            System.out.println("Dein USB Pfad wurde erfolgreich gespeichert.");
            System.out.println("Gib nun bitte deinen BackUP Pfad ein (z.B.: C:/Dokumente/MarvinIstCool/): ");
            Scanner backUpPathScanner = new Scanner(System.in);
            this.backUpPath = backUpPathScanner.next();
            System.out.println("Deine Eingabe wird verarbeitet..");
            System.out.println("Bitte starte gleich den Computer einmal neu um mich in betrieb zu nehmen.");

            this.jsonConfiguration.append("usbPath", this.usbPath);
            this.jsonConfiguration.append("backUpPath", this.backUpPath);
            this.jsonConfiguration.save();
            System.exit(0);
        }

        this.usbPath = this.jsonConfiguration.getString("usbPath");
        this.backUpPath = this.jsonConfiguration.getString("backUpPath");
    }


    private boolean ready() {
        return this.jsonConfiguration.get("usbPath") != null;
    }
}
