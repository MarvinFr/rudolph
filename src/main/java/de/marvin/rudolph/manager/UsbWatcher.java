package de.marvin.rudolph.manager;

import java.io.File;

public class UsbWatcher extends Thread {

    private static UsbWatcher instance;
    private File file;

    public UsbWatcher() {
        UsbWatcher.instance = this;

        this.file = new File(SetupManager.getInstance().getUsbPath());
        run();
    }

    @Override
    public void run() {

        System.out.println("Search USB..");

        if(pathExists()) {
            System.out.println("USB found!");
            System.out.println("Copy files..");
            new ProcessManager();
            currentThread().stop();
        }

        try {
            sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        run();
    }

    private boolean pathExists() {
        return this.file.exists();
    }
}
