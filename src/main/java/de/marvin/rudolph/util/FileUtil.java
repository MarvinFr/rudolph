package de.marvin.rudolph.util;

import java.io.File;
import java.io.IOException;

public class FileUtil {

    public static File createFile(String folderPath, String name) {
        File folder = new File(folderPath);
        if (!folder.exists())
            folder.mkdir();

        File file = new File(folder, name);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return file;
    }

    public static File createFile(String name) {
        File file = new File(name);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return file;
    }

    public static File createFolder(String folderPath) {
        File folder = new File(folderPath);
        if (!folder.exists())
            folder.mkdir();
        return folder;
    }

    public static void deleteFile(String folderPath, String name) {
        File file = new File(folderPath, name);
        if (!file.exists())
            return;
        file.delete();
    }

    public static void deleteFile(File file) {
        if (!file.exists())
            return;
        file.delete();
    }


}
