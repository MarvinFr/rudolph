package de.marvin.rudolph.util;

import com.google.common.collect.Sets;
import com.google.gson.*;
import lombok.Getter;

import java.io.*;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;

@Getter
public class JsonConfiguration {

    private final Gson gson;
    private final JsonParser jsonParser;
    private final JsonObject dataCache;

    private File file;

    public JsonConfiguration(File file, JsonObject jsonObject) {

        this.gson = new GsonBuilder().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();
        this.jsonParser = new JsonParser();
        this.file = file;
        this.dataCache = jsonObject;
    }

    public JsonConfiguration(File file) {

        this.gson = new GsonBuilder().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();
        this.jsonParser = new JsonParser();
        this.file = file;

        JsonObject dataCache = new JsonObject();
        try (InputStreamReader reader = new InputStreamReader(Files.newInputStream(file.toPath()), "UTF-8");
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            dataCache = this.jsonParser.parse(bufferedReader).getAsJsonObject();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (IllegalStateException | NullPointerException ignored) {
        }
        this.dataCache = dataCache;
    }

    public JsonConfiguration(JsonObject jsonObject) {

        this.gson = new GsonBuilder().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();
        this.jsonParser = new JsonParser();
        this.dataCache = jsonObject;
    }

    public JsonConfiguration() {

        this.gson = new GsonBuilder().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();
        this.jsonParser = new JsonParser();
        this.dataCache = new JsonObject();
    }

    public JsonConfiguration append(String property, JsonElement value) {
        this.dataCache.add(property, value);
        return this;
    }

    public JsonConfiguration append(String property, String value) {
        this.dataCache.addProperty(property, value);
        return this;
    }

    public JsonConfiguration append(String property, Number value) {
        this.dataCache.addProperty(property, value);
        return this;
    }

    public JsonConfiguration append(String property, Boolean value) {
        this.dataCache.addProperty(property, value);
        return this;
    }

    public JsonConfiguration append(String property, Character value) {
        this.dataCache.addProperty(property, value);
        return this;
    }

    public JsonConfiguration append(String property, List<JsonElement> value) {
        JsonArray jsonArray = new JsonArray();

        for (JsonElement jsonElement : value) {
            jsonArray.add(jsonElement);
        }

        this.dataCache.add(property, jsonArray);
        return this;
    }

    public JsonConfiguration append(String property, JsonConfiguration value) {
        this.dataCache.add(property, value.getDataCache());
        return this;
    }

    public JsonConfiguration append(String property, Object object) {
        this.dataCache.add(property, this.gson.toJsonTree(object));
        return this;
    }

    public JsonElement get(String property) {
        return this.dataCache.get(property);
    }

    public String getString(String property) {
        return this.dataCache.get(property).getAsString();
    }

    public Number getNumber(String property) {
        return this.dataCache.get(property).getAsNumber();
    }

    public Boolean getBoolean(String property) {
        return this.dataCache.get(property).getAsBoolean();
    }

    public Character getCharacter(String property) {
        return this.dataCache.get(property).getAsCharacter();
    }

    public JsonArray getJsonArray(String property) {
        return this.dataCache.get(property).getAsJsonArray();
    }

    public JsonConfiguration getDocument(String property) {
        return new JsonConfiguration(this.dataCache.get(property).getAsJsonObject());
    }

    public JsonConfiguration remove(String property) {
        this.dataCache.remove(property);
        return this;
    }

    public JsonConfiguration clear() {
        for (String property : this.properties()) {
            this.dataCache.remove(property);
        }
        return this;
    }

    public boolean isEmpty() {
        return this.dataCache.entrySet().size() == 0;
    }

    public int size() {
        return this.dataCache.entrySet().size();
    }

    public Set<String> properties() {
        Set<String> properties = Sets.newHashSet();
        this.dataCache.entrySet().forEach(entry -> properties.add(entry.getKey()));
        return properties;
    }

    public boolean contains(String property) {
        return this.dataCache.has(property);
    }

    public boolean save(File file) {
        if (file == null)
            return false;

        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8")) {
            this.gson.toJson(this.dataCache, writer);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean save() {
        if (this.file == null)
            return false;
        return this.save(this.file);
    }

    @Override
    public String toString() {
        return this.dataCache.toString();
    }
}

